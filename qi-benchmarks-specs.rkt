#lang racket/base

(require (for-syntax racket/base
                     syntax/parse
                     racket/syntax-srcloc
                     syntax/strip-context)
         racket/function
         vlibench)

(provide benchmarks-specs)

(define-syntax (define-qi-benchmark-specs stx)
  (syntax-parse stx
    ((_ spec-name
        ((impl-id impl-mod ...) ...)
        (flow-id flow-prepare flow-expr) ...)
     (with-syntax ((current-file-name
                    (datum->syntax #'spec-name
                                   (srcloc-source (syntax-srcloc #'spec-name))))
                   (((plain-impl-mod ...) ...) (strip-context #'((impl-mod ...) ...))))
       #`(begin
           (module impl-id racket/base
             (require vlibench)
             (require plain-impl-mod ...)
             (provide flow-id) ...
             (define this-impl-id (symbol->string 'impl-id))
             #,(with-syntax (((plain-flow-expr ...)
                              (replace-context #'this-impl-id #'(flow-expr ...))))
                 #'(begin
                     (define flow-id
                       (vlib/prog this-impl-id
                                  (flow plain-flow-expr)
                                  #'plain-flow-expr)) ...)))
           ...
           (dynamic-require (list 'submod current-file-name 'impl-id) #f) ...
           (define spec-name
               (list (let ((this-flow-id 'flow-id))
                       (vlib/spec this-flow-id flow-prepare
                                  (list (dynamic-require
                                         (list 'submod current-file-name 'impl-id)
                                         this-flow-id) ...)))
                     ...)))))))

(define-qi-benchmark-specs
  benchmarks-specs
  ((Qi-Deforested qi qi/list racket/math)
   (Qi-Core qi racket/list racket/math))
  (filter-map make-random-integer-list
                (~>> (filter odd?) (map sqr)))
  (filter-map-foldl make-random-integer-list
                     (~>> (filter odd?) (map sqr) (foldl + 0)))
  (long-pipeline identity
                  (~>> range
                       (filter odd?)
                       (map sqr)
                       values
                       (filter (lambda (v) (< (remainder v 10) 5)))
                       (map (lambda (v) (* v 2)))
                       (foldl + 0)))
  (range-map-car identity
                 (~>> range (map sqr) car))
  (range-map-take identity
                  (~>> (+ 100000) range (map sqr) (take _ 100000)))
  )
