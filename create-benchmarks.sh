#!/bin/sh

NAME=benchmarks-`date +%Y%m%dT%H%M%S`

PROFILE="preview"
if [ -n "$1" ] ; then
    PROFILE="$1"
fi

scribble \
    ++convert svg \
    ++arg -p \
    ++arg "$PROFILE" \
    --html \
    --dest results/$NAME \
    --dest-name index.html \
    report-qi.scrbl
