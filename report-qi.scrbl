#lang scribble/manual

@require[scribble-math/dollar
	 srfi/19
	 vlibench
	 (for-syntax racket/base)
	 racket/cmdline
	 racket/string
	 racket/function]

@;Command-line handling ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@(define config-profile
  (let ()
    (define profile-box (box 'preview))
    (command-line
      #:once-each
      (("-p" "--profile")
       name
       "profile name to use (use 'list' to list available profiles)"
       (when (equal? name "list")
	 (displayln
	  (format
	   "Available profiles: ~a"
	   (string-join
	    (for/list (((k v) vlib/profiles))
	      (symbol->string k))
	    ", ")))
	 (exit 0))
       (set-box! profile-box (string->symbol name))))
    (unbox profile-box)))

@title[#:style (with-html5 manual-doc-style)]{Qi Normal/Deforested Competitive Benchmarks}

@;Qi version helper ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@(begin-for-syntax
  (require setup/getinfo)
  (define (get-version)
    ((get-info '("qi")) 'version)))
@(define-syntax (get-qi-version stx)
  (datum->syntax stx (get-version) stx stx))

@;Specification ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@(require "qi-benchmarks-specs.rkt")

@; Processing ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@define[profile (hash-ref vlib/profiles config-profile)]
@(define results
  (for/list ((spec (in-list benchmarks-specs)))
    (run-benchmark
      spec
      #:profile profile)))

@; Rendering ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{General Information}

Date and time: @(date->string (seconds->date (current-seconds)))

@snippet:vlib/profile[profile]

@snippet:system-information[#:more-versions `(("Qi Version: " ,(get-qi-version)))]


@section{Summary Results}

@snippet:summary-results-table[results]


@section{Detailed Results}

@snippet:benchmark/s-duration[results]

Measured lengths: @(racket #,(for/list ((len (vlib/profile->steps profile))) len))

@(for/list ((result (in-list results)))
  (snippet:benchmark-result result))
